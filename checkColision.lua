require("lovedebug")


function love.load()
    --Create 2 rectangles
    r1 = {
        x = 10,
        y = 100,
        width = 100,
        height = 100
    }

    r2 = {
        x = 250,
        y = 120,
        width = 150,
        height = 120
    }
end

function love.update(dt)
    --Make one of rectangle move

    local tempR = table.shallow_copy(r1)
    
    tempR.x = tempR.x + 100 * dt


    if checkCollision(tempR, r2) then
        --If there is collision, draw the rectangles filled
    else
        r1.x = tempR.x
    end

end

function love.draw()
    --We create a local variable called mode
    local mode
    if checkCollision(r1, r2) then
        --If there is collision, draw the rectangles filled
        mode = "fill"
    else
        --else, draw the rectangles as a line
        mode = "line"
    end

    --Use the variable as first argument
    love.graphics.rectangle(mode, r1.x, r1.y, r1.width, r1.height)
    love.graphics.rectangle(mode, r2.x, r2.y, r2.width, r2.height)
end


function checkCollision(a, b)
    
    --With locals it's common usage to use underscores instead of camelCasing
    local a_left = a.x
    local a_right = a.x + a.width
    local a_top = a.y
    local a_bottom = a.y + a.height

    local b_left = b.x
    local b_right = b.x + b.width
    local b_top = b.y
    local b_bottom = b.y + b.height

    --If Red's right side is further to the right than Blue's left side.
    return a_right > b_left and
    --and Red's left side is further to the left than Blue's right side.
    a_left < b_right and
    --and Red's bottom side is further to the bottom than Blue's top side.
    a_bottom > b_top and
    --and Red's top side is further to the top than Blue's bottom side then..
    a_top < b_bottom

end

function table.shallow_copy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end